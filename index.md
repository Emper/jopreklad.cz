---
layout: home
---

# Angličtina na úrovni

Ahoj. Jmenuji se Jana Osičková a už léta se věnuji překládání firemních dokumentů a korespondence.


Kromě hovorové angličtiny se specializuji na **angličtinu technickou**, **lékařskou** a **chemickou**.


Jsem držitelem certifikátu CAE, studovala jsem na mezinárodní škole v Holandsku a kromě překladateství angličtinu také vyučuji.
