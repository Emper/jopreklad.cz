---
title: Kontakt
description: "Kontakt na překladatelku technické a medicínské angličtiny, dokumentů i korespondence."
layout: page
permalink: /kontakt/
---

Specializuji se na odbornou angličtinu (technickou, lékařskou a&nbsp;farmaceutickou). Pokud potřebujete přeložit dokumenty, korespondenci a&nbsp;nebo článek, **napište mi**. Rychle odpovím a&nbsp;rychle přeložím, už od 150,-&nbsp;Kč za normostranu.

## [{{ site.email }}](mailto:{{ site.email }}){: target="_blank"}


<div class="main-info">
  {% include social-links.html %}
</div>
