---
title: Reference
description: "Seznam referencí, zkušenosti s překládáním do angličtiny a vyukou angličtiny."
layout: page
permalink: /reference/
---

**2017 – dodnes**: Farmak a.s.

**2014 – dodnes**: Lektorky.cz – jazyková škola

**2015, 2016**: Nestlé Česko s.r.o. - překlady dokumentů

**2015, 2016**: Panorama ILA – jazyková škola

**2015, 2016**: Morxes a.s. - překlady dokumentů a korespondence

**2014, 2015, 2016**: Mezinárodní konference NANOCON

**2012**: Certifikát Cambridge CAE - úroveň C2
