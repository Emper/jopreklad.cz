---
layout: post
title: "Oslovení v&nbsp;korespondenci"
excerpt: "Správné oslovení patří mezi nejdůležitější formality dopisu."
description: "Správné oslovení v dopise či e-mailu je důležitá formalita a pravidla jsou přitom jednoduchá, naučte se rozdíl mezi Dear Mr., Mrs., Ms., Sir a Madam"
image: "images/post.jpg"
---

Oslovení v dopise patří mezi nejdůležitější formální požadavek, a to nejen v angličtině. Rozhodující je, jestli známe jméno adresované osoby. Tvar je vždy stejný, začínáme slovem "Dear" (drahý, milý, atd), poté vhodný tvar oslovení a řádek ukončíme čárkou.

Pokud známe přijmení osoby, oslovujeme například "Dear Mr. Novák," (milý pane Nováku). Další tvary jsou:

  - **Mr./Mister** (pán) - oslovený muž může být ženatý i svobodný
  - **Ms./Miss** (slečna) - oslovená žena je svobodná
  - **Mrs./Missus** (paní) - oslovená žena je vdaná


Pokud ale jméno dané osoby neznáme, používá se "Dear Sir or Madam," (milý pane nebo paní). Můžeme také použít lomítko místo slova "or", oba tvary jsou stejně formální. Možné tvary jsou tedy:

  - **Dear Sir/Madam** (milý pane/paní)
  - **Dear Sir or Madam** (milý pane nebo paní)

V případě, že sice znáte jméno příjemce, ale kvůli cizímu jménu budeme mít problém určit, jestli se jedná o muže nebo ženu, použijeme oslovení Sir/Madam.

Stejně jako v češtině pak věta pokračuje na novém řádku nebo odstavci. Nevhodné oslovení může příjemce znepokojit, proto se hodí začínat odchozí poštu správně. 
