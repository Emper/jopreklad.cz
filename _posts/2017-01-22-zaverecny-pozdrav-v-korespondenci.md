---
layout: post
title: "Závěrečný pozdrav"
excerpt: "Pozdrav nebo rozloučení v&nbsp;závěru dopisu může mít několik podob."
description: "Pozdravy a rozloučení anglicky v závěru e-mailové a dopisové korespondence. Angličtina, E-mail, příklady: Kind/Best regards, Yours faithfully/sincerely"
image: "images/post.jpg"
---

Pozdrav nebo rozloučení v&nbsp;závěru dopisu může mít mnoho podob. Mezi formální e-mailovou a&nbsp;klasickou dopisovou korespondencí přitom dnes už není skoro žádný rozdíl.

Rozdíl může být ale závislý na počátečním oslovení. Pokud jsme příjemce oslovili jménem, pak můžeme jako závěrečný pozdrav zvolit **"Yours sincerely"** (Se srdečným pozdravem). Pokud jsme ale oslovili obecně, například "Dear Sir/Madam", pak se jako pozdrav používá **"Yours faithfully"** (S úctou).

Dále existují i obecné a více familiérní pozdravy, které nejsou závislé na oslovení. Ty se obzvlášť v obchodních e-mailech používají velmi často, například:

  - **Regards** (S pozdravy)
  - **Best regards** (S nejlepšími pozdravy)
  - **Kind regards** (Srdečné pozdravy)
  - **With kind regards** (Se srdečnými pozdravy)


Hned po pozdravu nesmíme zapomenout čárku a podpis. Přitom podpis by měl být na novém řádku, popřípadě v úplně novém odstavci.

_Poznámka nakonec:_ Pokud očekáváme odpověď, můžeme ještě před záverečný pozdrav dodat větu **"I look forward to hearing from you"** (Těším se na Vaši odpověď).
